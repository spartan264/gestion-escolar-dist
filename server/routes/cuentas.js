const express = require('express');
const router = express.Router();

const Cuenta = require('../models/Cuenta');

router.post('/', async (req,res) => {
    const cuenta = new Cuenta(req.body);
    await cuenta.save();
    res.json({
        status: 'Cuenta guardada'
    });
});

module.exports = router;